module.exports = {
  secret: 'sssssh this is the jwt secret',
  options: {
    algorithm: 'HS256',
    expiresIn: '14d',
    issuer: 'api.transaction.com',
    audience: 'transaction.com',
  },
};
