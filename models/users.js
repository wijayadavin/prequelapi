const Sequelize = require('sequelize');
const db = require('../configs/dbConnection');

const users = db.define(
    'users',
    {
      id: {type: Sequelize.STRING,
        primaryKey: true},
      firstName: {type: Sequelize.STRING},
      lastName: {type: Sequelize.STRING},
      email: {type: Sequelize.STRING},
      username: {type: Sequelize.STRING},
      password: {type: Sequelize.STRING},
      role: {type: Sequelize.STRING},
    },
    {
      freezeTableName: true,
    },
);

module.exports = users;
